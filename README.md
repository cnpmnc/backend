# Cnpmnc backend

Flask

MySQL

## Setup Linux

```
sudo -H pip3 install virtualenv
virtualenv venv
source ./venv/bin/activate
pip install -r requirements.txt
```

## Setup Win

```
pip install -r requirements.txt
```

## Running

```
flask run
```

## Migration

```
flask db init
flask db migrate
flask db upgrade
```
