class Errors:
    success = 10
    failed = 11
    not_found = 12
    missing_field = 13
    logged = 14


error_code = Errors()
