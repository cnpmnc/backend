from os import environ
from dotenv import load_dotenv
load_dotenv()


class Config:
    """
    Set flask conf vars from .env file
    """

    # General
    FLASK_APP = environ["FLASK_APP"]
    TESTING = environ["TESTING"]
    FLASK_DEBUG = environ["FLASK_DEBUG"]
    SECRET_KEY = environ.get('SECRET_KEY')

    # Database
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
