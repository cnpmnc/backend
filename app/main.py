from __future__ import absolute_import

from flask import Flask
from flask_migrate import Migrate

from app.controllers.Users import users_blueprint
from app.controllers.Category import category_blueprint
from app.controllers.Books import books_blueprint

from app.models.Users import Users

from flask_login import LoginManager

from app.models import db

from configs.Config import Config

app = Flask(__name__)
login_manager = LoginManager()
login_manager.init_app(app)

@login_manager.user_loader
def user_loader(user_id):
    user = Users.query.filter_by(id=user_id).first()
    return user

app.config.from_object(Config)
db.init_app(app)

app.register_blueprint(users_blueprint, url_prefix="/users")
app.register_blueprint(category_blueprint, url_prefix="/category")
app.register_blueprint(books_blueprint, url_prefix="/books")

migrate = Migrate(app, db)
