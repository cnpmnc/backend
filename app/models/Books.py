from app.models import db


categories = db.Table('categories', db.Column('cate_id', db.Integer,
                                              db.ForeignKey('category.id'),
                                              primary_key=True),
                      db.Column('book_id', db.Integer,
                                db.ForeignKey('books.id'), primary_key=True))

class Books(db.Model):
    __tablename__ = 'books'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)
    author = db.Column(db.Text, nullable=False)
    url = db.Column(db.Text, nullable=False)
    description = db.Column(db.Text, nullable=False)
    pub_year = db.Column(db.String(4), nullable=False)
    state = db.Column(db.String(14), nullable=False)
    suitable_time = db.Column(db.DateTime, nullable=False)

    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    categories = db.relationship('Category', secondary=categories,
                                 lazy='subquery')
