from app.models import db


class Users(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    password = db.Column(db.Text, nullable=False)
    name = db.Column(db.Text, nullable=False)
    phone = db.Column(db.String(10), nullable=True)
    email = db.Column(db.String(100), nullable=False, unique=True)
    address = db.Column(db.Text, nullable=False)

    book = db.relationship("Books", backref="owner")