from __future__ import absolute_import

from flask import Blueprint, jsonify, request

from app.models.Category import Category
from app.models.Books import Books, categories

from app.models import db

from configs.Errors import error_code

from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime

category_blueprint = Blueprint('Category', __name__)

@category_blueprint.route("/", methods=['GET'])
def get_all_available_category():
    categories = Category.query.all()

    if not categories:
        response = {
                'success': "false",
                'message': error_code.not_found
            }

        return response

    response = {
            'success': "true",
            'message': error_code.success
        }

    response['data'] = []

    for category in categories:
        temp = category.__dict__
        del temp["_sa_instance_state"]

        response['data'].append(temp)

    return response

@category_blueprint.route('/', methods=['POST'])
def create_category():
    data = request.json

    category = Category.query.filter(Category.name == data['name']).first()

    print(category, flush=True)

    response = {
            'message': 'Category exist',
            'success': 'false'
        }

    if not category:
        new_category = Category(
            name = data['name']
        )

        try:
            db.session.add(new_category)
            db.session.commit()
            response = {
                'message': 'Add new category success',
                'success': 'true'
            }
        except Exception as e:
            db.session.rollback()
            print (e, flush=True)
            response = {
                'message': 'Cannot add new category',
                'success': 'false'
            }

    return jsonify(response)

@category_blueprint.route('/<int:id>', methods=['PUT'])
def update_category_by_id(id):
    category = Category.query.get(id)

    if not category:
        return jsonify({
            'message': 'Not found category.',
            'success': 'false'
        })

    print(request, flush=True)
    data = request.get_json()
    print(data, flush=True)

    if data['name']:
        category.name = data['name']

    try:
        db.session.commit()
        response = {
            'message': 'Updated category id {}'.format(category.id),
            'success': 'true'
        }
    except Exception as e:
        db.session.rollback()
        response = {
            'message': e,
            'success': 'false'
        }

    return response

@category_blueprint.route('/<int:id>', methods=['DELETE'])
def delete_category_by_id(id):
    category = Category.query.get(id)

    if not category:
        return jsonify({
            'message': 'Not found',
            'success': 'false'
        })

    try:
        db.session.delete(category)
        db.session.commit()
        response = {
            'message': 'Deleted category id {}'.format(category.id),
            'success': 'true'
        }
    except Exception as e:
        db.session.rollback()
        response = {
            'message': e,
            'success': 'false'
        }

    return response

# @category_blueprint.route('/catetobook', methods=['POST'])
# def category_to_book():
#     data = request.json

#     cate = Category.query.get(data['cate_id'])
#     book = Books.query.get(data['book_id'])

#     if not cate or not book:
#         response = {
#             'message': 'Request not valid',
#             'success': 'false'
#         }

#         return response

#     new_cat_to_book = categories(
#         cate_id = data['cate_id'],
#         book_id = data['book_id']
#     )

#     try:
#         db.session.add(new_cat_to_book)
#         db.session.commit()
#         response = {
#             'message': 'Request success',
#             'success': 'true'
#         }
#     except Exception as e:
#         db.session.rollback()
#         print (e, flush=True)
#         response = {
#             'message': 'Request unsuccess',
#             'success': 'false'
#         }

#     return response

# # @category_blueprint.route('/getcatbybook/<int:id>', methods=['GET'])
# # def get_category_by_book_id(id):
# #     categories_instances = categories.query.filter(book_id==id).all()

# #     if not categories_instances:
# #         response = {
# #                 'success': "false",
# #                 'message': error_code.not_found
# #             }

# #         return response

# #     response = {
# #             'success': "true",
# #             'message': error_code.success
# #         }

# #     response['data'] = []

# #     for categories_instance in categories_instances:
# #         temp = categories_instance.__dict__
# #         del temp["_sa_instance_state"]

# #         response['data'].append(temp)

# #     return response
