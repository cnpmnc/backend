from __future__ import absolute_import

from flask import Blueprint, jsonify, request
from datetime import datetime

from app.models.Books import Books

from app.models import db

from configs.Errors import error_code
import json
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime

books_blueprint = Blueprint('Books', __name__)

@books_blueprint.route("/available/", methods=['GET'])
def get_all_available_books():
    books = Books.query.filter(Books.state != 'DONE').all()
    print("query success")
    if not books:
        response = {
                'success': "false",
                'message': error_code.not_found
            }

        return response

    response = {
            'success': "true",
            'message': error_code.success
        }

    response['data'] = []

    for book in books:
        temp = book.__dict__
        del temp["_sa_instance_state"]

        response['data'].append(temp)

    return response

@books_blueprint.route('/', methods=['POST'])
def create_book():
    data = request.json
    
    try:
        new_book = Books(
            name = data['name'],
            author = data['author'],
            description = data['description'],
            pub_year = data['pub_year'],
            state = data['state'],
            url = data['url'],
            suitable_time = datetime.strptime(data['suitable_time'], '%m/%d/%y %H:%M:%S'),
            owner_id = data['owner_id']
        )
    except Exception as e:
        new_book = Books(
            name = "Harry Potter and the Philosophers Stone",
	        author =  "J. K. Rowling",
            description = "It's still new",
            pub_year = "2006",
            state =  "SELL",
            url =  "https://upload.wikimedia.org/wikipedia/vi/thumb/5/51/Harry_Potter_v%C3%A0_H%C3%B2n_%C4%91%C3%A1_ph%C3%B9_th%E1%BB%A7y_b%C3%ACa_2003.jpeg/250px-Harry_Potter_v%C3%A0_H%C3%B2n_%C4%91%C3%A1_ph%C3%B9_th%E1%BB%A7y_b%C3%ACa_2003.jpeg",
            suitable_time = datetime.strptime("05/29/15 05:50:00", '%m/%d/%y %H:%M:%S'),
            owner_id = "1"
        )

        print(e, flush=True)

        response = {
            'message': 'Cannot add new book',
            'success': 'false',
            'error': e
        }
         

    print(data, flush=True)

    try:
        db.session.add(new_book)
        db.session.commit()
        response = {
            'message': 'Add new book success',
            'success': 'true'
        }
    except Exception as e:
        db.session.rollback()
        print (e, flush=True)
        response = {
            'message': 'Cannot add new book',
            'success': 'false',
            'error': response.get('error') + e
        }

    return jsonify(response)

@books_blueprint.route('/<int:id>', methods=['PUT'])
def update_book_by_id(id):
    book = Books.query.get(id)

    if not book:
        return jsonify({
            'message': 'Not found book.',
            'success': 'false'
        })

    print(request, flush=True)
    data = request.get_json()
    print(data, flush=True)

    if data['name']:
        book.name = data['name']

    if data['author']:
        book.author = data['author']

    if data['description']:
        book.description = data['description']

    if data['url']:
        book.url = data['url']
    
    if data['pub_year']:
        book.pub_year = data['pub_year']

    if data['state']:
        book.state = data['state']

    if data['suitable_time']:
        book.suitable_time = datetime.strptime(data['suitable_time'], '%m/%d/%y %H:%M:%S')

    if data['owner_id']:
        book.owner_id = data['owner_id']

    try:
        db.session.commit()
        response = {
            'message': 'Updated book id {}'.format(book.id),
            'success': 'true'
        }
    except Exception as e:
        db.session.rollback()
        response = {
            'message': e,
            'success': 'false'
        }

    return response

@books_blueprint.route('/<int:id>', methods=['DELETE'])
def delete_book_by_id(id):
    book = Books.query.get(id)

    if not book:
        return jsonify({
            'message': 'Not found',
            'success': 'false'
        })

    try:
        db.session.delete(book)
        db.session.commit()
        response = {
            'message': 'Deleted book id {}'.format(book.id),
            'success': 'true'
        }
    except Exception as e:
        db.session.rollback()
        response = {
            'message': e,
            'success': 'false'
        }

    return response