from __future__ import absolute_import

from flask_login import login_user

from flask import Blueprint, jsonify, request, session

from app.models.Users import Users

from app.models import db

from configs.Errors import error_code

from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime

users_blueprint = Blueprint('Users', __name__)


@users_blueprint.route("/", methods=['GET'])
def get_all_users():
    users = Users.query.all()

    if not users:
        response = {
                'success': "false",
                'message': error_code.not_found
            }

        return response

    response = {
            'success': "true",
            'message': error_code.success
        }

    response['data'] = []

    for user in users:
        temp = user.__dict__
        del temp["_sa_instance_state"]
        del temp["password"]

        response['data'].append(temp)

    return response


@users_blueprint.route("/<string:email>", methods=['GET'])
def get_user_by_email(email):
    user = Users.query.filter_by(email=email).first()
    if not user:
        response = {
            'message': "User with email={} not found".format(email),
            'success': "false"
        }
    else:
        response = {
            'message': "Found user with email={}".format(email),
            'success': "true"
        }
        response['data'] = user.__dict__
        del response['data']["password"]
        del response['data']["_sa_instance_state"]

    return jsonify(response)

@users_blueprint.route('/register', methods=['POST'])
def create_user():
    data = request.json
    data['password'] = generate_password_hash(data['password'])

    new_user = Users(
        name=data['name'],
        phone=data['phone'],
        email=data['email'],
        address=data['address'],
        password=data['password']
    )

    try:
        db.session.add(new_user)
        db.session.commit()
        response = {
            'message': 'Register success',
            'success': 'true'
        }
    except Exception as e:
        db.session.rollback()
        response = {
            'message': 'Cannot register',
            'success': 'false'
        }

    return jsonify(response)


@users_blueprint.route('/<int:id>', methods=['PUT'])
def update_user_by_id(id):
    user = Users.query.get(id)

    if not user:
        return jsonify({
            'message': 'Not found',
            'success': 'false'
        })

    data = request.get_json()

    if data['name']:
        user.name = data['name']

    if data['phone']:
        user.phone = data['phone']

    if data['email']:
        user.email = data['email']

    if data['address']:
        user.address = data['address']

    try:
        db.session.commit()
        response = {
            'message': 'Updated user {}'.format(user.email),
            'success': 'true'
        }
    except Exception as e:
        db.session.rollback()
        response = {
            'message': e,
            'success': 'false'
        }

    return jsonify(response)


@users_blueprint.route('/<int:id>', methods=['DELETE'])
def delete_user_by_id(id):
    user = Users.query.get(id)

    if not user:
        return jsonify({
            'message': 'Not found',
            'success': 'false'
        })

    try:
        db.session.delete(user)
        db.session.commit()
        response = {
            'message': 'Deleted user {}'.format(user.email),
            'success': 'true'
        }
    except Exception as e:
        db.session.rollback()
        response = {
            'message': e,
            'success': 'false'
        }

    return jsonify(response)


@users_blueprint.route('/login', methods=['POST'])
def login():
    if 'logged' in session and session.get('logged'):
        return jsonify({
            'message': 'You have logged in.',
            'success': 'false'
        })

    data = request.json

    if data['email']:
        user = Users.query.filter_by(email=data['email']).first()
    else:
        return jsonify({
            'message': 'Missing username or email',
            'success': 'false'
        })

    if not check_password_hash(user.password, data['password']):
        return jsonify({
            'message': 'Wrong password',
            'success': 'false'
        })

    session['logged'] =  True
    session['uid'] = user.__dict__['id']

    return jsonify({
            'message': 'Login success',
            'success': 'true'
        })

@users_blueprint.route('/logout', methods=['POST', 'GET'])
def logout():
    if session.get('logged'):
        session['logged'] = False
        session['uid'] = 0

        return jsonify({
                'message': 'Logout success',
                'success': 'true'
            })

    return jsonify({
        'message': 'You have not logged in.',            
        'success': 'false'
    })

    